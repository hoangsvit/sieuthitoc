<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-06 18:17:24
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-06 19:53:12
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MY_Controller {

	public function __construct(){

		parent::__construct();
		//$this->load->model('Home_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Trang Chủ';
		$this->_data['loadPage']  = 'home/index';

		//$this->_data['building'] = $this->Building_Model->get_all_building();

		$this->load->view($this->_data['path'],$this->_data);

	}
	
}