<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-08 21:56:25
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 22:56:59
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('Home_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Thông Tin Cá Nhân';
		$this->_data['loadPage']  = 'profile/index';

		$this->load->view($this->_data['path'],$this->_data);

	}
}