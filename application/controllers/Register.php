<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-07 23:14:02
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 01:10:14
 */
class Register extends MY_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('Register_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Đăng Kí';
		$this->_data['loadPage']  = 'register/index';

		$this->load->view($this->_data['path'],$this->_data);

	}

	public function request()
	{
		
		if($this->input->is_ajax_request()){

			$data = array(
				'user_full_name' => $this->input->post('tenhienthi'),
				'phone_number'   => $this->input->post('sodienthoai'),
				'user_password'  => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'email'          => $this->input->post('email'),
			);

			$this->db->insert('tbl_user',$data);

			echo json_encode(array('status' => TRUE));

		}else{

			echo json_encode(array('status' => FALSE));

		}
	}

	public function check_phone()
	{
		if($this->input->is_ajax_request()){

			$sodienthoai = $this->input->post("sodienthoai");

			$phone = $this->Register_Model->check_phone($sodienthoai);

			if ($phone > 0) {
				echo(json_encode(false));
			}else{
				echo(json_encode(true));
			}

		}

	}

	public function check_email()
	{
		if($this->input->is_ajax_request()){

			$email = $this->input->post("email");

			$get_email = $this->Register_Model->check_email($email);

			if ($get_email > 0) {
				echo(json_encode(false));
			}else{
				echo(json_encode(true));
			}

		}

	}

}