<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-06 15:55:02
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-09 09:38:02
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	private $limit = 10;

	public function __construct(){

		parent::__construct();
		$this->load->model('Home_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Trang Chủ';
		$this->_data['loadPage']  = 'home/index';

		$this->_data['list_store'] = $this->Home_Model->get_list_store($this->limit,0);

		$this->load->view($this->_data['path'],$this->_data);

	}

	public function ajax_load(){

		if ($this->input->is_ajax_request()) {

			$page = $this->input->post("page");
			
			if(!empty($page)){

				$total = $this->Home_Model->count_all_store();

				$start = ceil($page * $this->limit);

				if ($total >= $start) {
					
					$data = $this->Home_Model->get_list_store($start,$this->limit);

					echo json_encode(array('status' => TRUE, 'data' => $data));

				}else{

					echo json_encode(array('status' => FALSE, 'error' => 'Not Found'));

				}

			}

		}

	}

	public function ajax_load_category(){

		if ($this->input->is_ajax_request()) {

			$id   = $this->input->post("id");
			$page = $this->input->post("page");
			
			if(!empty($page)){

				echo $total = $this->Home_Model->count_all_store_by_cate($id);
die;
				$start = ceil($page * $this->limit);

				if ($total >= $start) {
					
					$data = $this->Home_Model->get_list_store($start,$this->limit);

					echo json_encode(array('status' => TRUE, 'data' => $data));

				}else{

					echo json_encode(array('status' => FALSE, 'error' => 'Not Found'));

				}

			}

		}

	}

	public function get_category()
	{

		if($this->input->is_ajax_request()){

			$category = $this->Home_Model->get_all_category();

			echo json_encode(array('status' => TRUE, 'data' => $category));

		}else{

			echo json_encode(array('status' => FALSE, 'errors' => 'errors'));

		}

	}

	public function get_district()
	{

		if($this->input->is_ajax_request()){

			$district = $this->Home_Model->get_all_district();

			echo json_encode(array('status' => TRUE, 'data' => $district));

		}else{

			echo json_encode(array('status' => FALSE, 'errors' => 'errors'));

		}

	}

}