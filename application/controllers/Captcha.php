<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-07 23:23:10
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-07 23:28:57
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Captcha extends MY_Controller {

	public function __construct(){

		parent::__construct();


	}

	public function index()
	{

		$vals = array(
			'word'        => '',
			'img_path'    => './assets/captcha/',
			'img_url'     => base_url('assets/').'/captcha/',
			'font_path'   => base_url('assets/font').'/wetpet.ttf',
			'img_width'   => '150',
			'img_height'  => 30,
			'expiration'  => 7200,
			'word_length' => 5,
			'font_size'   => 18,
			'img_id'      => 'ramdom-captcha',
			'pool'        => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        // White background and border, black text and red grid
			'colors'     => array(
				'background' => array(255, 255, 255),
				'border'     => array(25, 63, 40),
				'text'       => array(0, 0, 0),
				'grid'       => array(245, 245, 245)
			)
		);

		$cap = create_captcha($vals);
		echo $cap['image'];
		echo '<input type="hidden" id="input_recaptcha" name="recaptcha" value="'.$cap['word'].'">';

	}

}
