<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-07 22:14:49
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 01:58:54
 */
class Login extends MY_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('Login_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Đăng Nhập';
		$this->_data['loadPage']  = 'login/index';

		if ($this->input->post()) {

			$phone    = $this->input->post("sodienthoai");
			$password = $this->input->post("password");

			if ($this->Login_Model->check_phone($phone) > 0) {

				$show = $this->Login_Model->get_password_by_phone($phone);

				if (password_verify($password, $show->user_password)) {

					$datasession = array(
						'user_id'      => $this->Login_Model->get_id_by_phone($phone)->user_id,
						'phone_number' => $phone,
						'email'        => $this->Login_Model->get_id_by_phone($phone)->email,
					);

					$this->session->set_userdata('login_user', $datasession);

					set_message('success', 'Đăng nhập thành công!');
					redirect(base_url());


				} else {

					$this->session->set_flashdata('login-error', 'Sai mật khẩu');
					set_message('error', 'Sai mật khẩu');
					redirect(base_url("login"));

				}

			}else {

				$this->session->set_flashdata('login-error', 'Tài khoản không tồn tại');
				set_message('error', 'Tài khoản không tồn tại');
				redirect(base_url("login"));

			}

		}

		$this->load->view($this->_data['path'],$this->_data);

	}

	public function logout()
	{
		set_message('success', 'Đăng xuất thành công!');
		$this->session->sess_destroy();
		$this->session->unset_userdata('login_user');
		redirect(base_url());

	}

}