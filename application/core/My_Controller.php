<?php

/**
 * @Author: David
 * @Date:   29-11-2017 17:38:17
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 22:42:58
 */
class MY_Controller extends CI_Controller
{

	protected $_data;
	public function __construct(){

        parent::__construct();

        if ($this->session->has_userdata('login_user')) {

        	$this->db->select('user_id, email, phone_number, user_full_name');
        	$this->db->where('user_id',$this->session->login_user['user_id']);
        	$this->_data['login_user'] = $this->db->get('tbl_user')->row();

        }

        $this->_data['path']    = "template";

    }

}