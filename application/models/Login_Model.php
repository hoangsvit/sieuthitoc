<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-08 01:38:21
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 01:51:47
 */
class Login_Model extends CI_Model{

	public function check_phone($phone){

		$this->db->where('phone_number', $phone);
		$result = $this->db->count_all_results('tbl_user');

		return $result;
	}

	public function get_password_by_phone($phone){
		$this->db->select('user_password');
		$this->db->where('phone_number', $phone);
		$query = $this->db->get('tbl_user');
		$result = $query->row();

		return $result;
	}

	public function get_id_by_phone($phone){
		$this->db->select('user_id,email');
		$this->db->where('phone_number', $phone);
		$query = $this->db->get('tbl_user');
		$result = $query->row();

		return $result;
	}

}