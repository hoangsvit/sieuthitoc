<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-06 16:04:12
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-09 09:43:14
 */
class Home_Model extends CI_Model{

	public function get_all_category(){
		$this->db->select('category_id, category_name');
        $query = $this->db->get('tbl_category');
        $result = $query->result();

        return $result;
	}

	public function get_all_district(){
		$this->db->select('district_id, district_name');
        $query = $this->db->get('tbl_district');
        $result = $query->result();

        return $result;
	}

	public function get_list_store($limit,$start)
	{
		$this->db->select('store_id,store_name,store_address,rating,order,photo,point');
		$this->db->limit($limit, $start);
		$query = $this->db->get('tbl_store');
		$result = $query->result();

		return $result;
	}

	public function count_all_store()
	{  
		$this->db->from('tbl_store');
		return $this->db->count_all_results();
	}

	public function count_all_store_by_cate($category_id)
	{  
		$this->db->where("FIND_IN_SET(".$category_id.",category_id)");
		$this->db->from('tbl_store');
		return $this->db->count_all_results();
	}

}