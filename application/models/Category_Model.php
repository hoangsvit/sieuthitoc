<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-09 09:28:59
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-09 09:30:09
 */
class Category_Model extends CI_Model{

	public function get_list_store($limit,$start,$category_id)
	{
		$this->db->select('store_id,store_name,store_address,rating,order,photo,point');
		$this->db->limit($limit, $start);
		$query = $this->db->get('tbl_store');
		$result = $query->result();

		return $result;
	}

	public function count_all_store()
	{  
		$this->db->from('tbl_store');
		return $this->db->count_all_results();
	}

}