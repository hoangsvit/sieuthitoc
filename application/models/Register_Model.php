<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-07 23:56:23
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 01:03:16
 */
class Register_Model extends CI_Model{

	public function check_phone($phone_number) {

        $this->db->where('phone_number', $phone_number);
        $result = $this->db->count_all_results('tbl_user');

        return $result;

    }

    public function check_email($email) {

        $this->db->where('email', $email);
        $result = $this->db->count_all_results('tbl_user');

        return $result;

    }

}