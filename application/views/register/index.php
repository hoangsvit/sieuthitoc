<div class="container">
	<div class="row d-flex justify-content-center text-center mb-5">
		<div class="col-md-5 mt-5">
			<h4 class="text-uppercase mb-3"><?= $titlePage ?></h4>
			<form method="POST" id="form-register" class="text-left">
				<div class="form-group">
					<label>Tên Hiển Thị</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-phone text text-orange"></i></span>
						</div>
						<input type="text" class="form-control" name="tenhienthi" placeholder="Tên hiển thị *">
					</div>
				</div>
				<div class="form-group">
					<label>Nhập số điện thoại để đăng nhập sử dụng dịch vụ</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-phone text text-orange"></i></span>
						</div>
						<input type="number" name="sodienthoai" class="form-control" placeholder="Số điện thoại *">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-lock text-orange"></i></span>
						</div>
						<input type="password" id="password" name="password" class="form-control" placeholder="Nhập mật khẩu *">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-lock text-orange"></i></span>
						</div>
						<input type="password" name="repassword" class="form-control" placeholder="Nhập lại mật khẩu *">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-envelope text-orange"></i></span>
						</div>
						<input type="email" name="email" class="form-control" placeholder="Nhập email *">
					</div>
				</div>

				<div id="check" class="form-group">
					<div class="input-group d-flex align-items-center">
						<span class="input-group-btn">
							<span id="image-captcha"></span>
						</span>
						<span class="input-group-btn">
							<a class="btn btn-danger refresh" href="javascript:void(0)" title="Refresh Captcha"><i class="fa fa-sync-alt"></i></a>
						</span>
						<input type="text" name="captcha" class="form-control" placeholder="Nhập mã xác nhận *">
					</div>
				</div>

				<button type="button" id="save_click" class="btn bg-orange btn-block text-white text-center">Đăng kí</button>
			</form>

		</div>
	</div>
</div>


<script>
	$(document).ready(function() {

		$("#save_click").click(function() {

			if($("#form-register").valid()){

				$.ajax({
                    type: "POST",
                    url: '<?= base_url('register/request') ?>',
                    dataType: "JSON",
                    data: $("#form-register").serialize(),
                    success: function (res) {

                        if (res.status) {

                            toastr.success('Đăng kí thành công !!!');
                            
                            setTimeout(function () {
                                window.location.href = '<?= base_url("login") ?>';
                            }, 3000);

                        } else {

                            toastr.error('Lỗi rồi, vui lòng thử lại !!!');

                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });

			}else{

				toastr.error('Vui lòng kiểm tra và thử lại !!!');
				$("#form-register").valid();

			}
		})

		$("#form-register").validate({
			rules: {
				sodienthoai: {
					rangelength: [10, 11],
					required: true,
					number: true,
					remote : {
						url: "<?= base_url('register/check_phone') ?>",
						type: "POST",
					}
				},
				tenhienthi: "required",
				password: {
					required: true,
					minlength: 6,
				},
				repassword: {
					required: true,
					minlength: 6,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true,
					remote : {
						url: "<?= base_url('register/check_email') ?>",
						type: "POST",
					}
				},
				captcha: {
					required: true,
					equalTo: "#input_recaptcha"
				},

			},
			messages : {
				'sodienthoai': {
					remote: 'Số điện thoại đã tồn tại, vui lòng nhập số khác',
				},
				'email': {
					remote: 'Email đã tồn tại, vui lòng nhập email khác hoặc có thể bấm quên mật khẩu',
				},
				'captcha': {
					equalTo: 'Vui lòng nhập đúng với hình ảnh bên cạnh',
				},
				'repassword': {
					equalTo: 'Vui lòng nhập đúng với mật khẩu phía trên',
				},
			},

			errorPlacement: function ( error, element ) {
				error.addClass( "invalid-feedback" );

				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				}else if(element[0] == "input#phonenumber"){
					error.insertAfter( element.parent( "m-input-group--air" ) );
				} else {
					error.insertAfter( element );
					console.log(element);
				}

			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );

			}
		});


	// Load captcha image.    
	$( "#image-captcha" ).load( "<?= base_url('captcha') ?>" );    
	// Ajax post for refresh captcha image.
	$("a.refresh").click(function() {

	// Begin Chage Button
	$('.refresh').html('<i class="fa fa-sync-alt fa-spin"></i>');
	$('.refresh').attr('disabled',false);
	// End Change Button
	jQuery.ajax({
		type: "POST",
		url: "<?= base_url('captcha') ?>",
		success: function(res) {
			if (res)
			{
				setTimeout(function(){

						// Begin Chage Button
						$('.refresh').html('<i class="fa fa-sync-alt"></i>');
						$('.refresh').attr('disabled',false);
						// End Change Button
						$("#image-captcha").html(res);

					}, 500);
			}
		}
	});
}); 
});
</script>