<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-06 16:04:36
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 23:04:21
 */
?>
<!doctype html>
<html lang="vi">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Demo">

	<title><?= $titlePage ?></title>

	<!-- Bootstrap core CSS -->

	<link href="<?= base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

	<!-- Icon Fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- Custom CSS -->
	<link href="<?= base_url('assets/bootstrap/css/custom.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	
	<!-- Script need load first -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="<?= base_url('assets/js/custom-jquery-valid.js') ?>"></script>

</head>
<nav class="navbar navbar-expand navbar-dark bg-dark fixed-top fix-height-nav d-flex">

	<div class="px-2 d-none d-sm-block">
		<a href="javascript:;">
			<img class="icon-menu sidebar-tab-open" src="https://demo-ssl.engma.com.vn/public/img/icon/menu.png">
		</a>
	</div>

	<div class="px-2">

		<a class="navbar-brand ml-2" href="<?= base_url() ?>">
			<img src="https://demo-ssl.engma.com.vn/public/img/logo_r_02.png" class="img-fluid logo">
		</a>
	</div>

	<div class="box-menu-total ">
		<div class="collapse navbar-collapse justify-content-center">

			<div class="tab-outlight-menu text-center">
				<a class="box-outlight float-left text-white link-outlight-active link-underline" href="<?= base_url() ?>">Cơ sở</a>
				<a class="box-outlight float-right text-orange" href="<?= base_url('employee') ?>">Nhân viên</a>
			</div>

		</div>
	</div>

</nav>

<div class="row sub-menu ml-0 mr-0 text-center fixed-sub-top">
	<div class="<?= check_employee_menu($this->uri->segment(1)) ?> menu-sub-item menu-border-right" onclick="open_menu(this.id,this.getAttribute('data'))" data="0" id="danh-gia">
		<a class="text-dark" href="javascript:;">
			<span class="menu-content">Đánh giá</span>
			<i class="fa fa-angle-down break-down-item"></i>
		</a>

	</div>

	<?php if ($this->uri->segment(1) !== 'employee'): ?>
		<div class="col-sm-4 menu-sub-item" onclick="open_menu(this.id,this.getAttribute('data'))" data="0" id="danh-muc">
			<a class="text-dark" href="javascript:;">
				<span class="menu-content">Danh Mục</span>
				<i class="fa fa-angle-down break-down-item"></i>
			</a>
		</div>
	<?php endif ?>

	<div class="<?= check_employee_menu($this->uri->segment(1)) ?> menu-sub-item menu-border-left" onclick="open_menu(this.id,this.getAttribute('data'))" data="0" id="quan-huyen">
		<a class="text-dark" href="javascript:;" id="quan-huyen">
			<span class="menu-content">TP.HCM</span>
			<i class="fa fa-angle-down break-down-item"></i>
		</a>
	</div>
</div>
<!-- Begin Menu List Group -->
<div class="list-group fixed-drop-top scroll-list-group"></div>
<!-- Begin Menu List Group -->

<!-- Begin Sidebar Left -->
<div class="sidebar">
	<div class="sidebar-tab-close">
		<i class="fa fa-times fa-2x pt-3 pl-4 icon-close-sidebar"></i>
	</div>
	<div class="d-flex flex-column custom-sidebar pt-3">
		<a href="javascript:;" class="text-dark">
			Gần Tôi
		</a>
		<a href="javascript:;" class="text-dark">
			Danh Mục
		</a>

		<a href="javascript:;" class="text-dark">
			Cơ sở nổi bật
		</a>

		<a href="javascript:;" class="text-dark">
			Khuyến Mãi
		</a>
	</div>
</div>
<!-- End Sidebar Left -->

<!-- Begin Sidebar Right -->
<div class="sidebar-right text-center">
	<div class="sidebar-box d-block mb-2">
		<img width="51" src="https://demo-ssl.engma.com.vn/public/img/icon/new/home.png">
		<a href="<?= base_url() ?>" class="position-absolute text-white">
			Trang Chủ
		</a>
	</div>

	<div class="sidebar-box d-block mb-2">
		<img width="51" src="https://demo-ssl.engma.com.vn/public/img/icon/new/overlap.png">
		<a href="javascript:;" class="position-absolute text-white">
			Hoạt động
		</a>
	</div>

	<div class="sidebar-box d-block mb-2">
		<img width="51" src="https://demo-ssl.engma.com.vn/public/img/icon/new/file.png">
		<a href="javascript:;" class="position-absolute text-white">
			Lịch sử đặt hẹn
		</a>
	</div>

	<div class="sidebar-box d-block mb-2">
		<img width="51" src="https://demo-ssl.engma.com.vn/public/img/icon/new/bell.png">
		<a href="javascript:;" class="position-absolute text-white">
			Thông báo
		</a>
	</div>

	<div class="sidebar-box d-block mb-2">
		<img width="51" src="https://demo-ssl.engma.com.vn/public/img/icon/new/user.png">
		<a href="<?= base_url('profile') ?>" class="position-absolute text-white">
			<?= ($this->session->has_userdata('login_user')) ? $login_user->user_full_name : 'Tài khoản' ?>
		</a>
	</div>

</div>
<!-- End Sidebar Right -->
<body class="normal bg-light">


