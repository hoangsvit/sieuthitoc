<ul class="list-group">
	<a href="<?= ($this->session->has_userdata('login_user')) ? 'javascript:;' : base_url('login') ?>" class="list-group-item text-white mb-2 pt-0 pb-0" style="background-color: #d4892e !important">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/user.png" style="background-color: unset;border: unset;">
		<span class="pl-2"><?= ($this->session->has_userdata('login_user')) ? $login_user->user_full_name : 'Đăng nhập tài khoản' ?></span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<a href="<?= base_url() ?>" class="list-group-item text-dark">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/home.png">
		<span class="pl-2">Trang Chủ</span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<a href="<?= base_url() ?>" class="list-group-item text-dark">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/overlap.png">
		<span class="pl-2">Hoạt Động</span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<a href="javascript:;" class="list-group-item text-dark">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/bell_01.png">
		<span class="pl-2">Thông báo</span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<a href="javascript:;" class="list-group-item text-dark mb-4">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/file.png">
		<span class="pl-2">Lịch sử đặt dịch vụ </span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<?php if ($this->session->has_userdata('login_user')): ?>
	<a href="<?= base_url('logout') ?>" class="list-group-item text-dark">
		<img src="https://demo-ssl.engma.com.vn/public/img/setting/logout.png" style="border: 3px solid #a9a9a9;background-color: #a9a9a9;">
		<span class="pl-2">Đăng xuất </span>
		<span class="float-right"><i class="fa fa-angle-right"></i></span>
	</a>
	<?php endif ?>
</ul>

