<!-- Begin Slider -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="https://demo-ssl.engma.com.vn/public/upload/banner/e234ae-Banner-My-pham-toc-Paul-Mitchell.jpg" alt="First slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="https://demo-ssl.engma.com.vn/public/upload/img/banner/2.jpg" alt="Second slide">
		</div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<!-- End Slider -->
<div class="list-group d-flex flex-row">
	<a href="javascript:;" id="find_me" class="list-group-item list-group-item-action m-2">
		<img src="https://demo-ssl.engma.com.vn/public/img/icon/gantoi.png" style="background-color: unset;border: unset;">
		<span class="pl-2">Gần tôi</span>
	</a>
	<a href="#" class="list-group-item list-group-item-action m-2">
		<img src="https://demo-ssl.engma.com.vn/public/img/icon/danhmuc.png" style="background-color: unset;border: unset;">
		<span class="pl-2">Danh Mục</span>
	</a>
</div>
<div class="list-group d-flex flex-row">
	<a href="#" class="list-group-item list-group-item-action m-2">
		<img src="https://demo-ssl.engma.com.vn/public/img/icon/noibat.png" style="background-color: unset;border: unset;">
		<span class="pl-2">Cơ sở nổi bật</span>
	</a>
	<a href="#" class="list-group-item list-group-item-action m-2">
		<img src="https://demo-ssl.engma.com.vn/public/img/icon/khuyenmai.png" style="background-color: unset;border: unset;">
		<span class="pl-2">Khuyễn Mãi</span>
	</a>
</div>

<!-- Begin Load Content -->
<div class="ml-2 mr-2">
	<div class="row">

		<?php foreach ($list_store as $store): ?>

			<div class="col-md-6">
				<div class="card mb-4 shadow-sm">
					<div class="card-body d-flex align-items-center">
						<div class="pl-2 d-flex flex-column">
							<span class="store-point"><?= $store->point ?></span>

						</div>
						<div class="pl-2 d-flex flex-column">
							<span class="font-weight-bold"><?= $store->store_name ?></span>
							<span style="color: #777;font-size: 11px;"><?= $store->store_address ?></span>
						</div>
					</div>
					<img class="card-img-top" src="<?= $store->photo ?>" data-holder-rendered="true" style="height: 409px; width: 100%; display: block;">
					<div class="card-body">
						<div class="d-flex align-items-center">
							<a href="javascript:void(0)" class="pr-2">
								<img src="https://demo-ssl.engma.com.vn/public/img/icon/elections.png" style="width: 16px;height: 16px;">
								<span class="totalcomment-title">Đánh giá</span>
								<span class="total-count"><?= $store->rating ?></span>
							</a>
							<a href="javascript:void(0)" class="pl-2">
								<img src="https://demo-ssl.engma.com.vn/public/img/icon/landat.png"  style="width: 16px;height: 16px;">
								<span class="totalcomment-title">Lần đặt</span>
								<span class="total-count"><?= $store->order ?></span>
							</a>
							<small class="ml-auto"><span class="total-count">Đóng cửa</span><span class=" inactive-shop-dot ml-2"></span></small>
						</div>
					</div>
				</div>
			</div>

		<?php endforeach ?>

		<!-- Begin Ajax Load -->
		<div class="ajax-load"></div>
		<!-- End Ajax Load -->

	</div>
</div>

<!-- End Load Content -->

<script>
	var page = 1;
	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			page++;
			loadMoreData(page);
		}
	});

	function loadMoreData(page){
		$.ajax(
		{
			url: '<?= base_url('ajax-load') ?>',
			type: "POST",
			data: {
				page: page,
			},
			dataType: "json",
			success: function(res){
				
				if(res.status){

					var html = '';

					$.each(res.data, function(index, value) {

						html += '<div class="col-md-6">';
						html += '<div class="card mb-4 shadow-sm">';
						html += '<div class="card-body d-flex align-items-center">';
						html += '<div class="pl-2 d-flex flex-column">';
						html += '<span class="store-point">'+value['point']+'</span>';

						html += '</div>';
						html += '<div class="pl-2 d-flex flex-column">';
						html += '<span class="font-weight-bold">'+value['store_name']+'</span>';
						html += '<span style="color: #777;font-size: 11px;">'+value['store_address']+'</span>';
						html += '</div>';
						html += '</div>';
						html += '<img class="card-img-top" src="'+value['photo']+'" data-holder-rendered="true" style="height: 409px; width: 100%; display: block;">';
						html += '<div class="card-body">';
						html += '<div class="d-flex align-items-center">';
						html += '<a href="javascript:void(0)" class="pr-2">';
						html += '<img src="https://demo-ssl.engma.com.vn/public/img/icon/elections.png" style="width: 16px;height: 16px;">';
						html += '<span class="totalcomment-title">Đánh giá</span>';
						html += '<span class="total-count">'+value['rating']+'</span>';
						html += '</a>';
						html += '<a href="javascript:void(0)" class="pl-2">';
						html += '<img src="https://demo-ssl.engma.com.vn/public/img/icon/landat.png"  style="width: 16px;height: 16px;">';
						html += '<span class="totalcomment-title">Lần đặt</span>';
						html += '<span class="total-count">'+value['order']+'</span>';
						html += '</a>';
						html += '<small class="ml-auto"><span class="total-count">Đóng cửa</span><span class=" inactive-shop-dot ml-2"></span></small>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';

					})

					$('.ajax-load').after(html);

				}else{

					toastr.error('Đã lấy hết dữ liệu !!!');

				}

			}
		})
		/*.done(function(data)
		{
			if(data == " "){
				$('.ajax-load').html("No more records found");
				return;
			}
			$('.ajax-load').hide();
			$("#post-data").append(data);
		})*/
		.fail(function(jqXHR, ajaxOptions, thrownError)
		{
			alert('server not responding...');
		});
	}
</script>