<div class="container">
	<div class="row d-flex justify-content-center text-center">
		<div class="col-md-5 mt-5">
			<h4 class="text-uppercase mb-3"><?= $titlePage ?></h4>

			<button type="button" class="btn btn-primary btn-block text-left" style="background-color: #4267b2;border-color: #4267b2;"><i class="fa fa-facebook"></i> Đăng nhập bằng Facebook</button>

			<button type="button" class="btn btn-primary btn-block text-left" style="background-color: #db4437; border-color: #db4437;"><i class="fa fa-google"></i> Đăng nhập bằng Google</button>


			<h5 class="mt-3 mb-3">Hoặc</h5>
			<form id="form-login" method="POST" class="text-left">


				<?php if(!empty($this->session->flashdata('login-error'))){ ?>
					<div class="alert alert-danger alert-dismissible fade show m-alert m-alert--air">
						<button class="close" data-close="alert" ></button> <?php echo $this->session->flashdata('login-error'); ?>
					</div>
				<?php } ?>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-phone text text-orange"></i></span>
						</div>
						<input type="number" name="sodienthoai" class="form-control" placeholder="Số Điện Thoại *" value="<?= set_value('sodienthoai') ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-lock text-orange"></i></span>
						</div>
						<input type="password" name="password" class="form-control" placeholder="******">
					</div>
				</div>
				<button type="submit" class="btn bg-orange btn-block text-white">Đăng nhập</button>
			</form>
			
			<div class="mt-5">
				<p class="mb-0"><a href="javascript:;">Quên mật khẩu?</a></p>
				<p>Bạn chưa có tài khoản? <a class="" href="<?= base_url('register') ?>">Đăng ký</a></p>
			</div>

		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$("#form-login").validate({
			rules: {
				sodienthoai: {
					rangelength: [10, 11],
					required: true,
					number: true
				},
				password: {
					required: true,
					minlength: 6,
				},
			},

			errorPlacement: function ( error, element ) {
				error.addClass( "invalid-feedback" );

				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				}else if(element[0] == "input#phonenumber"){
					error.insertAfter( element.parent( "m-input-group--air" ) );
				} else {
					error.insertAfter( element );
					console.log(element);
				}

			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );

			}
		});
	});
</script>