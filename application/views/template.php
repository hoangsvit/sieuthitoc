<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-06 16:04:56
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-06 16:05:24
 */

$this->load->view("header");
$this->load->view($loadPage);
$this->load->view("footer");