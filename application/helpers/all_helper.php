<?php

/**
 * @Author: David
 * @Date:   03-11-2017 17:27:49
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-08 22:50:24
 */
if (!defined('BASEPATH'))

    exit('No direct script access allowed');

if (!function_exists('check_login')) {

    function check_login()

    {

        if ($this->session->has_userdata('login_user')) {

            return TRUE;

        }else{

           return FALSE; 

        }

    }

}


if (!function_exists('check_employee')) {

    function check_employee_menu($data)

    {

        if ($data == 'employee') {

            return 'col-md-6';

        }else{

           return 'col-md-4'; 

        }

    }

}

if(!function_exists("get_qrcode")){

	function get_qrcode($data){

		$qrcode = file_get_contents(base_url("api/phpQRAPI/generateqr.php?userIdSerialNo='$data'"));

		return './'.$qrcode;

	}
}

if(!function_exists("get_qrcode_key")){

	function get_qrcode_key($data,$rename,$color){

		$qrcode = file_get_contents(base_url("api/phpQRAPI/generateqr.php?userIdSerialNo='$data'"));

		$class   = (!empty($color)) ? 'm--font-danger' : '';
		$popover = (!empty($color)) ? 'data-toggle="m-popover" data-html="true" data-content="This image is Master QRCode.<br>Click automatic downloading image." data-original-title="Glossary"' : 'data-toggle="m-popover" data-html="true" data-content="This image is QRCode.<br>Click automatic downloading image." data-original-title="Glossary"';

		return '<a href="'.base_url($qrcode).'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger" download="'.$rename.'" '.$popover.'><i class="fa fa-qrcode '.$class.'" style="font-size: 2rem !important;"></i></a>';

	}
}


if (!function_exists('time_elapsed_string')) {
    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if (!function_exists('get_name')) {

    function get_name($id)

    {

        // Begin Call Database
        $ci=& get_instance();
        $ci->load->database();
        // End Call Database
        $user = $ci->db->select('name')->where('user_id', $id)->get('tbl_users')->row();

        if (count($user) > 0) {

            return $user->name;

        }else{

            return 'N/A';

        }

    }

}

if (!function_exists('get_organization_id')) {

    function get_organization_id($building_id)

    {

        // Begin Call Database
        $ci=& get_instance();
        $ci->load->database();
        // End Call Database
        $organization_id = $ci->db->select('organization_id')->where('building_id', $building_id)->get('tbl_building')->row()->organization_id;

        if (count($organization_id) > 0) {

            return $organization_id;

        }else{

            return '';

        }

    }

}

if (!function_exists('get_organization')) {

    function get_organization($id)

    {
        if (isset($id)) {

             // Begin Call Database
            $ci=& get_instance();
            $ci->load->database();
            // End Call Database
            $organization_id = $ci->db->select('organization_id')->where('building_id', $id)->get('tbl_building')->row();

            $organization_name = $ci->db->select('organization_name')->where('organization_id', $organization_id->organization_id)->get('tbl_organization')->row();

            if (count($organization_name->organization_name) > 0) {

                return $organization_name->organization_name;

            }else{

                return 'NULL';

            }

        }

    }

}

if (!function_exists('get_master_key')) {

	function get_master_key($id)

	{
		if (isset($id)) {

			// Begin Call Database
			$ci=& get_instance();
			$ci->load->database();
			// End Call Database
			$user         = $ci->db->select('organization_id')->where('user_id', $id)->get('tbl_users')->row();
			$organization = $ci->db->select('master_key')->where('organization_id', $user->organization_id)->get('tbl_organization')->row();

			if (count($organization->master_key) > 0) {

				return $organization->master_key;

			}else{

				return 'NULL';

			}

		}

	}

}

if (!function_exists('check_status')) {

    function check_status($id)

    {

        // Begin Call Database
        $ci=& get_instance();
        $ci->load->database();
        // End Call Database

        // Get lastest online of device
        $res_date = $ci->db->select('time')->where('device_id', $id)->order_by('device_log_id', 'DESC')->limit(1)->get('tbl_devices_log')->row();
        //$get_date = $res_date->time;

        if($ci->db->affected_rows() >0){

            $time = strtotime($res_date->time);

            $now = strtotime("now");

            $check = $now - $time;

            if($check > 30){

                return '<span class="m-badge m-badge--danger">Offline ('.time_elapsed_string($res_date->time).')</span>';

            }else{

                return '<span class="m-badge m-badge--success">Online</span>';

            }


        }else{

            return '<span class="m-badge m-badge--secondary">Not Check</span>';

        }

    }

}

if (!function_exists('check_status_style2')) {

    function check_status_style2($id)

    {

        // Begin Call Database
        $ci=& get_instance();
        $ci->load->database();
        // End Call Database

        // Get lastest online of device
        $res_date = $ci->db->select('time')->where('device_id', $id)->order_by('device_log_id', 'DESC')->limit(1)->get('tbl_devices_log')->row();
        //$get_date = $res_date->time;

        if($ci->db->affected_rows() >0){

            $time = strtotime($res_date->time);

            $now = strtotime("now");

            $check = $now - $time;

            if($check > 30){

                return '<span class="m-badge m-badge--secondary"></span> '. time_elapsed_string($res_date->time);

            }else{

                return '<span class="m-badge m-badge--success"></span>';

            }


        }else{

            return '<span class="m-badge m-badge--danger"></span>';

        }

    }

}

if (!function_exists('check_status_style3')) {

    function check_status_style3($id)

    {

        // Begin Call Database
        $ci=& get_instance();
        $ci->load->database();
        // End Call Database

        // Get lastest online of device
        $res_date = $ci->db->select('time')->where('device_id', $id)->order_by('device_log_id', 'DESC')->limit(1)->get('tbl_devices_log')->row();
        //$get_date = $res_date->time;

        if($ci->db->affected_rows() >0){

            $time = strtotime($res_date->time);

            $now = strtotime("now");

            $check = $now - $time;

            if($check > 30){

                return '<span class="m-badge m-badge--danger"></span> '. time_elapsed_string($res_date->time);

            }else{

                return '<span class="m-badge m-badge--success"></span>';

            }


        }else{

            return '<span class="m-badge m-badge--secondary"></span>';

        }

    }

}


if (!function_exists('check_device_test_page')) {

    function check_device_test_page($data)

    {

        if ($data == 0) {

            return '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">Fail</span>';

        }else{

           return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Pass</span>'; 

        }

    }

}

if (!function_exists('check_step')) {

    function check_step($total,$step,$status)

    {
        if($status > 0){
            if ($total == $step) {

                return 'false';

            }elseif($total > $step){

                return 'true'; 

            }
        }
        

    }

}

if(!function_exists('check_step_status')) {
    function check_step_status($step, $total) {

        if(isset($step->test_report) && $step->test_report === '0') {
            return false;
        }else

        return true;
    }
}

if(!function_exists('progressbar')) {
	function progressbar($step) {

		if(!empty($step)) {

			return ($step / 7) * 100;

		}else

			return 100;
	}
}

if (!function_exists('check_device_test')) {

    function check_device_test($data)

    {

        if ($data == 0) {

            return '<span class="m-badge m-badge--danger">Fail</span>';

        }elseif($data == 1){

            return '<span class="m-badge m-badge--success">Pass</span>'; 
            
        }else{

            return '<span class="m-badge m-badge--secondary">Not Test</span>';   

        }

    }

}

if (!function_exists('check_special')) {

    function check_special($data)

    {

        if ($data == 0) {

            return 'User Reject';

        }elseif($data == 1){

            return 'Profile (FR)'; 
            
        }else{

            return 'No Profile (QRCode)';   

        }

    }

}



if (!function_exists('check_menu_active')) {

    function check_menu_active($url,$url_current)

    {

        if ($url == $url_current) {

            return 'm-menu__item--active';

        }

    }

}


if (!function_exists('check_admin_avatar')) {

    function check_admin_avatar($file_name_avatar)

    {

        if (!empty($file_name_avatar)) {

            return base_url('api/'.$file_name_avatar);

        }else{

            return base_url('api/avatars/users/default.png');

        }

    }

}


if (!function_exists('check_role')) {

    function check_role($data)

    {

        if ($data == 0) {

            return 'Super Admin';

        }elseif($data == 1){

            return 'Admin';

        }elseif($data == 2){

            return 'Contractor';

        }elseif($data == 3){

	        return 'Trainer';

        }else{

            return 'Grassroot';

        }

    }

}


if (!function_exists('check_superadmin')) {

    function check_superadmin($data)

    {

        if ($data == 0) {

            return TRUE;

        }elseif($data == 1){

            return FALSE;

        }

    }

}

if (!function_exists('check_role_admin')) {

	function check_role_admin($data)

	{

		if ($data == 0 || $data == 1) {

			return TRUE;

		}else{

			return FALSE;

		}

	}

}


if (!function_exists('check_company')) {

    function check_company($data)

    {

        if ($data == 1) {

            return 'Admin';

        }elseif($data == 2){

            return 'Company';

        }else{

            return 'Member';

        }

    }

}

/* Function used to encode value */
if(!function_exists('encode_value'))
{
    function encode_value($value='')
    {
        if($value != '')
        {
            return str_replace('=','',base64_encode($value));
        }
    }
}
/* Function used to decode for encoded value */
if(!function_exists('decode_value'))
{
    function decode_value($value='')
    {
        if($value != '')
        {
            return base64_decode($value);
        }
    }
}


/* Function send email */
if(!function_exists('users_send_email'))
{
    function users_send_email($email, $subject, $msg)
    {

        $CI = & get_instance();
        $CI->load->library('email'); // load library 

        $config['protocol'] = 'sendmail';
        $config['charset']  = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $CI->email->initialize($config);
        $CI->email->from('info@qrdoors.com');
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($msg);

        if (!$CI->email->send())
        {
            echo $CI->email->print_debugger();
        }

    }
}

if(!function_exists('check_menu_b_f_r'))
{
    function check_menu_b_f_r($url)
    {

        if ($url == 'building' || $url == 'floor' || $url == 'room') {

            return 'm-menu__item--open m-menu__item--expanded';

        }

    }
}

if(!function_exists("format_date_sql_strtotime")){

    function format_date_sql_strtotime($date){

        if(isset($date)){

            return strtotime(str_replace('/','-',$date));

        }else{

            return '1970-01-01';

        }
        
    }
}

if(!function_exists("format_date_sql")){

    function format_date_sql($date){

        $date = str_replace('/', '-', $date);

        if(isset($date)){
            
            return date('Y/m/d H:i', strtotime($date));

        }else{

            return '1970-01-01';

        }
        
    }
}

if(!function_exists("u_format_date")){

    function u_format_date($date){

        if(isset($date)){

            return date('d/m/Y H:i',strtotime($date));

        }else{

            return '';

        }
        
    }
}


if(!function_exists("u_format_datetime")){

    function u_format_datetime($date){

        if(isset($date)){

            // Begin Format Date
            $year          = substr($date, 0, 4);
            $month         = substr($date, 4, 2);
            $day           = substr($date, 6, 2);       
            $original_date = strtotime($year.'-'.$month.'-'.$day);
            $new_date      = date('m/d/Y', $original_date);  
            // End Format Date
            // Begin Format Time
            $hour          = substr($date, 8, 2);
            $minutes       = substr($date, 10, 2);
            $second        = substr($date, 12, 2);
            $original_time = strtotime($hour.':'.$minutes.':'.$second);
            $new_time      = date('H:i:s', $original_time);
            // End Format Time
            return $new_date.' '.$new_time;

        }else{

            return '';

        }
        
    }
}

if(!function_exists("u_format_datetime_picker")){

    function u_format_datetime_picker($date){

        if(isset($date)){

            // Begin Format Date
            $year          = '20'.substr($date, 0, 2);
            $month         = substr($date, 2, 2);
            $day           = substr($date, 4, 2);          
            $original_date = $month.'/'.$day.'/'.$year;
            // End Format Date
            // Begin Format Time
            $hour          = substr($date, 6, 2);
            $minutes       = substr($date, 8, 2);
            $original_time = $hour.':'.$minutes;
            // End Format Time
            return $original_date.' '.$original_time;

        }else{

            return '';

        }
        
    }
}

if(!function_exists("u_format_sql_datetime")){

    function u_format_sql_datetime($date){

        if(isset($date)){

            // Begin Format Date
            $month    = substr($date, 0, 2);
            $day      = substr($date, 3, 2);
            $year     = substr($date, 8, 2);
            $new_date = $year.$month.$day;
            // End Format Date
            // Begin Format Time
            $hour     = substr($date, 11, 2);
            $minutes  = substr($date, 14, 2);
            $new_time = $hour.$minutes;
            // End Format Time
            return $new_date.$new_time;

        }else{

            return '';

        }
        
    }
}

if(!function_exists("check_datetime")){

    function check_datetime($date){

        if(isset($date)){

            // Begin Format Date
            $year          = '20'.substr($date, 0, 2);
            $month         = substr($date, 2, 2);
            $day           = substr($date, 4, 2);          
            $original_date = $month.'/'.$day.'/'.$year;
            // End Format Date
            // Begin Format Time
            $hour          = substr($date, 6, 2);
            $minutes       = substr($date, 8, 2);
            $original_time = $hour.':'.$minutes;
            // End Format Time
            // Begin Check Datetime
            $expired = (time() > strtotime($original_date .' '. $original_time ));
            if ($expired){
                return '<span class="m-badge m-badge--danger m-badge--wide">Expired</span>';
            }else{
                return '<span class="m-badge m-badge--success m-badge--wide">Enable</span>';
            }
            // End Check Datetime

           

        }else{

            return '';

        }
        
    }
}

if(!function_exists('send_email_template_attach')) {
	function send_email_template_attach($params)
	{

		$config = array();
		$setting_array = array();

		$CI = & get_instance();
		$CI->load->library('email');

		$settings_obj = $CI->db->get('tbl_settings')->result_array();

		if(!empty($settings_obj)):

			foreach ($settings_obj as $key => $value) {
				$setting_array[$value['key']] = $value['value'];
			}

		endif;


		// If using SMTP
		if ($setting_array['email_protocol'] == 2) {

			$config = array(

				'smtp_host' => $setting_array['email_smtp_host'],

				'smtp_port' => $setting_array['email_smtp_port'],

				'smtp_user' => $setting_array['email_smtp_user'],

				'smtp_pass' => $setting_array['email_smtp_pass'],

				'protocol' => $setting_array['email_protocol'],

			);

		}

		// Config email
		$config['mailtype'] = "html";

		$config['newline'] = "\r\n";

		$config['charset'] = 'utf-8';

		$config['wordwrap'] = TRUE;

		$CI->email->initialize($config);

		$CI->email->from($setting_array['from_email'], $setting_array['from_name']);

		$CI->email->subject($params['subject']);

		$CI->email->message($params['message']);

		$CI->email->attach($params['attach']);

		// Send Email HTML
		$CI->email->set_mailtype("html");
		// Send Email
		$CI->email->to($params['recipient']);

		$send = $CI->email->send();

		if ($send) {

			return $send;

		} else {

			$error = show_error($CI->email->print_debugger());

			return $error;

		}

	}
}


if(!function_exists('send_email_template')) {
    function send_email_template($params)
    {

        $config = array();
        $setting_array = array();

        $CI = & get_instance();
        $CI->load->library('email');

        $settings_obj = $CI->db->get('tbl_settings')->result_array();

        if(!empty($settings_obj)):

            foreach ($settings_obj as $key => $value) {
                $setting_array[$value['key']] = $value['value'];
            }

        endif;


        // If using SMTP
        if ($setting_array['email_protocol'] == 2) {

            $config = array(

                'smtp_host' => $setting_array['email_smtp_host'],

                'smtp_port' => $setting_array['email_smtp_port'],

                'smtp_user' => $setting_array['email_smtp_user'],

                'smtp_pass' => $setting_array['email_smtp_pass'],

                'protocol' => $setting_array['email_protocol'],

            );

        }

        // Config email
        $config['mailtype'] = "html";

        $config['newline'] = "\r\n";

        $config['charset'] = 'utf-8';

        $config['wordwrap'] = TRUE;

        $CI->email->initialize($config);

        $CI->email->from($setting_array['from_email'], $setting_array['from_name']);

        $CI->email->subject($params['subject']);

        $CI->email->message($params['message']);

        // Send Email HTML
        $CI->email->set_mailtype("html");
        // Send Email
        $CI->email->to($params['recipient']);

        $send = $CI->email->send();

        if ($send) {

            return $send;

        } else {

            $error = show_error($CI->email->print_debugger());

            return $error;

        }

    }
}

if(!function_exists('format_number_phone')) {
    function format_number_phone($phone_number) {
        if(  preg_match( '/^\+(\d{2})(\d{4})(\d{4})$/', $phone_number,  $matches ) )
        {
            $result = '+'.$matches[1] . ' ' .$matches[2] . ' ' . $matches[3];
            return $result;
        }
    }
}




