<?php

/**
 * @Author: Hoang Nguyen
 * @Date:   2018-10-09 09:27:38
 * @Last Modified by:   Hoang Nguyen
 * @Last Modified time: 2018-10-09 09:28:49
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	private $limit = 10;

	public function __construct(){

		parent::__construct();
		$this->load->model('Category_Model');

	}

	public function index()
	{

		$this->_data['titlePage'] = 'Danh Mục';
		$this->_data['loadPage']  = 'category/index';

		$this->_data['list_store'] = $this->Home_Model->get_list_store($this->limit,0);

		$this->load->view($this->_data['path'],$this->_data);

	}

	public function ajax_load(){

		if ($this->input->is_ajax_request()) {

			$page = $this->input->post("page");
			
			if(!empty($page)){

				$total = $this->Home_Model->count_all_store();

				$start = ceil($page * $this->limit);

				if ($total >= $start) {
					
					$data = $this->Home_Model->get_list_store($start,$this->limit);

					echo json_encode(array('status' => TRUE, 'data' => $data));

				}else{

					echo json_encode(array('status' => FALSE, 'error' => 'Not Found'));

				}

			}

		}

	}

}