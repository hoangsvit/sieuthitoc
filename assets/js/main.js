/*
* @Author: Hoang Nguyen
* @Date:   2018-10-06 22:05:56
* @Last Modified by:   Hoang Nguyen
* @Last Modified time: 2018-10-08 23:22:00
*/

$("#find_me").click(function () {
	if ("geolocation" in navigator){

		navigator.geolocation.getCurrentPosition(function(position){

			console.log("Lat: "+position.coords.latitude+" - Lang:"+ position.coords.longitude);

		});

	}else{
		console.log("Browser doesn't support geolocation!");
	}
});

$(".sidebar-box").hover(function(){
	
	$(this).attr("style", "width: 250px; background: #d4892e;");

}, function(){

	$(this).removeAttr("style");
});

function open_menu(id,data) {

	if (id == 'danh-muc') {

		if(data == 0){

			$( "#danh-muc" ).attr('data',1).addClass('bg-white');
			$( "#quan-huyen,#danh-gia" ).attr('data',0).removeClass('bg-white');

			$.ajax({
				type:'POST',
				url:'danh-muc',
				dataType:'json',
				cache: false,
				success:function(results){

					if(results.status){
						var html = '<a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop active"> Danh Mục <span class="fa fa-check pull-right"></span> </a>'; $.each(results.data, function (i, fb) {

							html+= '<a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark">';
							html+= fb.category_name;
							html+= '</a>';

						});

						$('.fixed-drop-top').html(html);

					}else{

						console.log(results.errors)

					}

				}
			});

		}else{

			$( "#danh-gia,#danh-muc,#quan-huyen" ).attr('data',0).removeClass('bg-white');
			$( '.fixed-drop-top' ).empty();

		}

	}else if(id == 'quan-huyen'){

		if(data == 0){

			$( "#quan-huyen" ).attr('data',1).addClass('bg-white');
			$( "#danh-muc,#danh-gia" ).attr('data',0).removeClass('bg-white');

			$.ajax({
				type:'POST',
				url:'quan-huyen',
				dataType:'json',
				cache: false,
				success:function(results){

					if(results.status){
						var html = '<a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop active"> TP.HCM <span class="fa fa-check pull-right"></span> </a>'; 
						$.each(results.data, function (i, fb) {

							html+= '<a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark">';
							html+= fb.district_name;
							html+= '</a>';

						});

						$('.fixed-drop-top').html(html);

					}else{

						console.log(results.errors)

					}

				}
			});

		}else{

			$( "#danh-gia,#danh-muc,#quan-huyen" ).attr('data',0).removeClass('bg-white');
			$( '.fixed-drop-top' ).empty();

		}


	}else{

		if (data == 0) {


			$( "#danh-gia" ).attr('data',1).addClass('bg-white');
			$( "#danh-muc,#quan-huyen" ).attr('data',0).removeClass('bg-white');

			var html = '<a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop active"> Đánh giá <span class="fa fa-check pull-right"></span> </a> <a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark"> Gần tôi </a> <a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark"> Giá từ cao đến thấp </a> <a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark"> Giá từ cao đến thấp </a> <a href="javascript:;" class="list-group-item d-flex justify-content-between align-items-center menu-drop text-dark"> Điểm đánh giá cao </a>';
			$( '.fixed-drop-top' ).html(html);

		}else{

			$( "#danh-gia,#danh-muc,#quan-huyen" ).attr('data',0).removeClass('bg-white');
			$( '.fixed-drop-top' ).empty();

		}

	}

}

$(document).ready(function() {


	jQuery('.sidebar-tab-close').click(function()
	{
		$('.sidebar').attr("style","width: 0%; height: 100%");
		
	});

	jQuery('.sidebar-tab-open').click(function()
	{
		$('.sidebar').attr("style","width: 20%; height: 100%");
		
	});

	

});