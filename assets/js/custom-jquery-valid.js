/*
* @Author: Hoang Nguyen
* @Date:   2018-10-08 00:24:36
* @Last Modified by:   Hoang Nguyen
* @Last Modified time: 2018-10-08 01:00:10
*/
$(document).ready(function() {
	$.extend( $.validator.messages, {
		required: "Vui lòng không bỏ trống.",
		maxlength: $.validator.format( "Hãy nhập ít {0} kí tự trở xuống." ),
		minlength: $.validator.format( "Hãy nhập ít nhất {0} kí tự trở lên." ),
		rangelength: $.validator.format( "Hãy nhập từ {0} đến {1} số." ),
		email: "Hãy nhập Email",
		url: "Hãy nhập URL.",
		date: "Hãy nhập ngày.",
		number: "Hãy nhập số.",
		digits: "Hãy nhập chữ số.",
		equalTo: "Hãy nhập thêm lần nữa.",
		range: $.validator.format( "Hãy nhập từ {0} đến {1}." ),
		max: $.validator.format( "Hãy nhập từ {0} trở lên." ),
		min: $.validator.format( "Hãy nhập từ {0} trở xuống." ),
		creditcard: "Hãy nhập số thẻ tín dụng."
	})
});